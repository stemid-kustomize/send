# Firefox Send

Kustomize deployment of the new and forked version of ["Firefox" Send](https://gitlab.com/timvisee/send).

# Overlay example

See the ``kustomize/overlays/example`` dir for an example.
